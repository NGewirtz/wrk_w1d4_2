require_relative 'board'
require_relative 'tile'
require "byebug"
class Game
  attr_accessor :board

  def initialize(board=Board.new)
    @board = board
  end

  def parse_pos(pos)
    pos.split(',').map(&:to_i)
  end

  def play
    board.place_bombs
    get_pos
  end

  def get_pos
    board.render
    puts "enter position (i.e '0,0')"
    guess = parse_pos(gets)
    lose if bomb?(guess)
  end

  def valid_pos?(pos)
    true
  end

  def bomb?(pos)
    puts board[pos]
  end

  # def [](pos)
  #   x,y = pos
  #   board[x][y]
  # end
  #
  # def []=(pos, value)
  #   x,y = pos
  #   board[x][y] = value
  # end

  def lose
    puts "Game Over"
  end

end

x = Game.new
x.play
