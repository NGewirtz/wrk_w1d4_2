class Tile

  attr_accessor :display, :bomb

  def initialize
    @display = "*"
    @bomb = false
  end

  def to_s
    display
  end
end
