require_relative 'tile'
require "byebug"
class Board
  attr_accessor :grid

  def initialize
    @grid = Array.new(9) do
      Array.new(9) { Tile.new }
    end
  end

  def render
    grid.each do |row|
      puts "#{row.join(" ")}"
    end
  end


  def place_bombs
    120.times do |tile|
      grid.flatten.sample.bomb = true
    end
  end

  def reveal

  end

  def [](pos)
    debugger
    x, y = pos
    grid[x][y]
  end

  def []=(pos, value)
    x, y = pos
    grid[x][y] = value
  end

end

puts x = Board.new
x.render
